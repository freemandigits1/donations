from django.contrib import admin

from meetup.models import Meetup


@admin.register(Meetup)
class MeetupAdmin(admin.ModelAdmin):
    list_display = ['location', 'meetup_date', 'contact_name', 'contact_phone', 'active']

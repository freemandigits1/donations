from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save
from django.conf import settings
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


def upload_location(instance, filename):
    return "%s/%s" % (instance.id, filename)


class CharityProfile(models.Model):
    organisation_category = (
        ('Charity','Charity'),
        ('NGO', 'NGO'),
        )
    user = models.ForiegnKey(settings.AUTH_USER_MODEL)
    name_of_organisation = models.CharField(max_length=200)
    category = models.CharField(max_length=50, choices=organisation_category)
    banner = models.ImageField(upload_to = upload_location, blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    state = models.CharField(max_length=40)
    address = models.CharField(max_length = 120, blank=True, null=True)

    def __unicode__(self):
        return self.user.username


class Campaign(models.Model):
    user = models.ForiegnKey(settings.AUTH_USER_MODEL)
    donor = models.ForiegnKey("Donor")
    description = models.TextField()

    def __unicode__(self):
        return self.user


class Donor(models.Model):
    name_or_organisation = models.CharField(max_length=150)
    email = models.EmailField(blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    amount = models.DecimalField(decimal_places=2, max_digits=20)

    def __unicode__(self):
        return self.name_or_organisation

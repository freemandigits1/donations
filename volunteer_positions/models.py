from __future__ import unicode_literals

from django.db import models


class Position(models.Model):
    nitch = models.CharField(max_length=200)
    when = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.nitch
